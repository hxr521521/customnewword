$(function() {

    init();
    $("#loginBtn").click(function() {

        var data = {
            username: $("#loginUsername").val(),
            password: $("#loginPassword").val()
        }
        login(data);

    });

    $("#regiesterBtn").click(function() {
        var data = {
            username: $("#registerUsername").val(),
            password: $("#registerPassword").val()
        }
        chrome.runtime.sendMessage({ action: "register", data: data },
            function(rs) {
                if (rs.status) { //注册成功
                    //自动登陆
                    login(data);
                    $("#registerTips").text("");
                } else {
                    $("#registerTips").text(rs.data);
                }
            });
    });


    //翻转后台的翻译状态
    $("#isStop").click(function() {
        chrome.runtime.sendMessage({ action: "flipStopFlag" },
            function(ok) {
                setTextIsStopBtnValue();
            });
    });

    $("#loginOut").click(function() {
        if (window.confirm("确定要退出登陆吗?")) {

            chrome.runtime.sendMessage({ action: "loginOut" },
                function(ok) {
                    init();
                });
        }
    });

});

//根据后台的是否翻译状态来设置按钮的值
function setTextIsStopBtnValue() {
    chrome.runtime.sendMessage({ action: "getStopFlag" },
        function(isStop) {
            if (isStop) {
                $("#isStop").val("开始翻译");
            } else {
                $("#isStop").val("停止翻译");
            }
        });
}

function login(data) {

    chrome.runtime.sendMessage({ action: "login", data: data },
        function(rs) {
            if (rs.status) { //登陆成功
                $("#loginTips").text("");
                init();
            } else {
                $("#loginTips").text(rs.data);
            }
        });
}

function init() {
    chrome.runtime.sendMessage({ action: "isLogin" },
        function(isLogin) {
            if (isLogin) { //已经登陆
                //设置翻译按钮
                setTextIsStopBtnValue();
                $("#configDiv").show();
                $("#loginDiv").hide();
                $("#registerDiv").hide();
            } else { //没有登陆 隐藏翻译按钮 显示登陆和注册div
                $("#configDiv").hide();
                $("#loginDiv").show();
                $("#registerDiv").show();
            }
        });
}

/**
 * 存储一个兼keyvalue到本地回话
 * @param key
 * @param value
 */
function putStorageJSON(key, value) {
    sessionStorage[key] = JSON.stringify(value);
}

/**
 * 获取一个本地会话的value
 * @param key
 * @returns
 */
function getStorageJSON(key) {
    if (sessionStorage[key]) {
        return JSON.parse(sessionStorage[key]);
    }
    return null;
}
