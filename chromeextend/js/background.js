var COMMON_URL = "https://customnewword.appspot.com/";
var isStop = true; //是否停止翻译
var target = null; //用户登陆唯一标识
var wordList = []; //缓存词组
chrome.runtime.onMessage.addListener(
    function(param, sender, sendResponse) {
        var func = actionUrlFunc[param.action];
        if (func) {
            sendResponse(func(param.data));
        } else {
            console.log('不存在的url' + param.action);
            alert('不存在的url' + param.action);
        }
    });

var actionUrlFunc = {
    "getWordList": getCacheWordList, //获取单词列表
    "putWordToSever": putWordToSever, //添加一个单词
    "flipStopFlag": function() { //翻转是否停止翻译状态
        isStop = !isStop;
        return true;
    },
    "getStopFlag": function() { //获取当前翻译状态
        return isStop;
    },
    "login": login, //注册
    "register": register, //登陆
    "isLogin": function() {
        return target == null ? false : true;
    },
    "loginOut": function() {
        isStop = true;
        target = null;
        return true;
    },
    "translation": translation
}

function getCacheWordList() {
    return wordList;
}

/**
获取服务器的单词列表
*/
function getWordList() {
    var result = byAjaxSync("getWordList", null);
    if (result != null && result.status) {
        return result.data;
    } else {
        return [];
    }
}

/**
上传一个单词
*/
function putWordToSever(word) {
    var data = {
        word: word
    }

    var rs = byAjaxSync("putWord", data);
    if (rs != null & rs.status) {
        wordList.push(word);
    }
    return rs;
}


function login(data) {
    var rs = byAjaxSync("login", data);
    if (rs == null) {
        return { status: false }
    }
    if (rs.status) {
        target = rs.data;
        //同时拉取缓存的词组
        wordList = getWordList();
    }
    return rs;
}

function register(data) {
    return byAjaxSync("registUser", data);
}

function translation(data) {
    var result = null;
    $.ajax({
        type: "get",
        async: false,
        url: "http://fanyi.youdao.com/openapi.do?keyfrom=hxr521521app&key=764649343&type=data&doctype=json&version=1.1&q=" + data,
        dataType: "json",
        success: function(word) {
            result = word;
        }
    });
    return result;
}

/**
一个同步ajax, 返回json
*/
function byAjaxSync(aciton, data) {

    if (data == null) {
        data = { target: target };
    } else {
        data.target = target;
    }

    var result = null;
    $.ajax({
        type: "post",
        async: false,
        url: COMMON_URL + aciton,
        data: data,
        dataType: "json",
        success: function(json) {
            result = json;
        }
    });
    return result;
}

/**
异步ajax
*/
function byAjaxYiBu(action, data, func) {
    if (data == null) {
        data = { target: target };
    } else {
        data.target = target;
    }
    $.ajax({
        type: "post",
        async: false,
        url: COMMON_URL + aciton,
        data: data,
        dataType: "json",
        success: function(json) {
            if (json.status) {
                if (func != null) {
                    func(json.data);
                }
            } else {
                console.log('server handler error ！！！');
            }
        }
    });
}
