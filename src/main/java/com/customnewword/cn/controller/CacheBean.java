package com.customnewword.cn.controller;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class CacheBean {
	
	private String systemPassword;
	
	/**
	 * key md5 value wordList
	 */
	private Map<String, Set<String>> cacheWordSet = new ConcurrentHashMap<String, Set<String>>();
	
	/**
	 * key username , value password
	 */
	private Map<String, String> cacheUserMap = new ConcurrentHashMap<String, String>();
	
	/**
	 * key 用户名 value 用户的md5
	 */
	private Map<String, String> cacheUserMd5 = new ConcurrentHashMap<String, String>();
	
	public Map<String, Set<String>> getCacheWordSet() {
		return cacheWordSet;
	}

	public void setCacheWordSet(Map<String, Set<String>> cacheWordSet) {
		this.cacheWordSet = cacheWordSet;
	}

	public Map<String, String> getCacheUserMap() {
		return cacheUserMap;
	}

	public void setCacheUserMap(Map<String, String> cacheUserMap) {
		this.cacheUserMap = cacheUserMap;
	}

	public Map<String, String> getCacheUserMd5() {
		return cacheUserMd5;
	}

	public void setCacheUserMd5(Map<String, String> cacheUserMd5) {
		this.cacheUserMd5 = cacheUserMd5;
	}

	public String getSystemPassword() {
		return systemPassword;
	}

	public void setSystemPassword(String systemPassword) {
		this.systemPassword = systemPassword;
	}
	
}
