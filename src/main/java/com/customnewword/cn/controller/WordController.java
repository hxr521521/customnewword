package com.customnewword.cn.controller;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.customnewword.cn.utils.MD5Util;

@RestController
public class WordController {
	
	private CacheBean cacheBean = new CacheBean();
	
	private Log log = LogFactory.getLog(WordController.class);
	
	@Value("${systemPassword}")
	private String systemPassword;

	@RequestMapping("/")
	String home() {
		log.info("into index");
		return getSuccessResultJson("hello world !!!");
	}
	
	@RequestMapping("/putWord")
	String putWord(String target, String word){
		log.info("into putWord word id [" + word +"]");
		if(StringUtils.isNotBlank(word) && StringUtils.isNotBlank(target)){
			Set<String> wordSet = cacheBean.getCacheWordSet().get(target);
			if(wordSet != null){
				wordSet.add(word);
				return getSuccessResultJson("putWord OK");
			}
		}
		return getErrorResultJson("putWord Error");
	}
	
	@RequestMapping("/getWordList")
	String getWordList(String target){
		log.info("getWordList");
		return getSuccessResultJson(cacheBean.getCacheWordSet().get(target));
	}
	
	/**
	 * 注册
	 * @param userName
	 * @param password
	 * @return
	 */
	@RequestMapping("/registUser")
	String registUser(String username, String password){
		log.info("into registUser  username : " + username);
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password)){
			return getErrorResultJson("用户名或密码不能为空");
		}
		
		if(cacheBean.getCacheUserMap().containsKey(username)){
			return getErrorResultJson("用户名已存在");
		}
		cacheBean.getCacheUserMap().put(username, password);
		String userMD5 = MD5Util.strToMD5(username + "*****myword***" + password);
		cacheBean.getCacheUserMd5().put(username, userMD5);
		cacheBean.getCacheWordSet().put(userMD5, new HashSet<String>());
		
		return getSuccessResultJson("注册成功");
	}
	
	/**
	 * 登陆
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/login")
	String login(String username, String password){
		log.info("into login" + username);
		if(StringUtils.isBlank(username) || StringUtils.isBlank(password)){
			return getErrorResultJson("用户名或密码不能为空");
		}
		
		String cachePassWord = cacheBean.getCacheUserMap().get(username);
		
		if(password.equals(cachePassWord)){//登陆成功返回md5唯一键
			return getSuccessResultJson(cacheBean.getCacheUserMd5().get(username));
		}else{
			return getErrorResultJson("账户或密码错误");
		}
	}
	
	/**
	 * 导出
	 * @param p
	 * @return
	 */
	@RequestMapping(value="exportAll", method=RequestMethod.GET)
	String exportAll(String p){
		if(systemPassword.equals(p)){
			return getSuccessResultJson(this.cacheBean);
		}else{
			return getErrorResultJson("未知错误");
		}
	}
	
	/**
	 * 导入
	 * @return
	 */
	@RequestMapping(value="importAll", method=RequestMethod.POST)
	String importAll(@RequestBody CacheBean tempCacheBean){
		if(cacheBean != null){
			if(this.systemPassword.equals(tempCacheBean.getSystemPassword())){
				//开始导入
				
				//首先导入cacheUserMap
				Map<String, String> tempCacheUserMap = tempCacheBean.getCacheUserMap();
				if(tempCacheUserMap != null){
					Map<String, String> cacheUserMap = this.cacheBean.getCacheUserMap();
					for(String tempUserName : tempCacheUserMap.keySet()){
						if(!cacheUserMap.containsKey(tempUserName)){
							cacheUserMap.put(tempUserName, tempCacheUserMap.get(tempUserName));
						}
					}
				}
				
				//导入用户名和md5
				Map<String, String> tempUserMd5 = tempCacheBean.getCacheUserMd5();
				if(tempUserMd5 != null){
					Map<String, String> cacheUserMd5 = this.cacheBean.getCacheUserMd5();
					for(String tempUserName : tempUserMd5.keySet()){
						if(!cacheUserMd5.containsKey(tempUserName)){
							cacheUserMd5.put(tempUserName, tempUserMd5.get(tempUserName));
						}
					}
				}
				//导入单词
				Map<String, Set<String>> tempUserWordSet = tempCacheBean.getCacheWordSet();
				if(tempUserWordSet != null){
					Map<String, Set<String>> userWordSetMap = this.cacheBean.getCacheWordSet();
					for(String md5 : tempUserWordSet.keySet()){
						Set<String> tempWordSet = tempUserWordSet.get(md5);
						if(tempWordSet != null){
							Set<String> userWordSet = userWordSetMap.get(md5);
							if(userWordSet == null){
								userWordSet = new HashSet<String>();
								userWordSetMap.put(md5, userWordSet);
							}
							userWordSet.addAll(tempWordSet);
						}		
					}
				}
				
				return getSuccessResultJson("导入成功");
			}
		}
		
		return getErrorResultJson("未知错误");
	}
	
	/**
	 *设置成功返回的json
	 * @param obj
	 * @return
	 */
	private String getSuccessResultJson(Object obj){
		JSONObject jo = new JSONObject();
		jo.put("status", true);
		jo.put("data", obj);
		
		return JSON.toJSONString(jo);
	}
	
	/**
	 * 设置处理失败返回的json
	 * @param obj
	 * @return
	 */
	private String getErrorResultJson(Object obj){
		JSONObject jo = new JSONObject();
		jo.put("status", false);
		jo.put("data", obj);
		
		return JSON.toJSONString(jo);
	}
	
	
	
	
}
